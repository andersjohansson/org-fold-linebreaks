;;; org-fold-linebreaks.el --- Display one sentence per line as multiple sentences.  -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2022  Anders Johansson

;; Author: Anders Johansson <anders.l.johansson@chalmers.se>
;; Keywords: wp
;; Created: 2020
;; Modified: 2022-09-13

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'org)

(defgroup org-fold-linebreaks nil
  "Customization of org fold linebreaks."
  :group 'org)

(defvar org-fold-linebreaks-re nil)
(defvar-local org-fold-linebreaks--overlays nil)

(defun org-fold-linebreaks--set-re (var val)
  "Setter for VAR ‘org-fold-linebreaks-sentence-end-punctuation’.
Constructs ‘org-fold-linebreaks-re’ from VAL."
  (set-default var val)
  (setq org-fold-linebreaks-re
        (rx
         (group
          (group
           ;; the punctuation
           (regexp (concat "[" (mapconcat #'car val) "]")))
          (group (? (any "\"”’"))) ;; possible closing quote
          "\n")
         (not (any "-\n* #")))))


(defcustom org-fold-linebreaks-sentence-end-punctuation
  '(("." . "·")
    ("?" . "?")
    ("!" . "!"))
  "Punctuation characters viewed as ending a sentence.
An alist of the characters (can be strings) that should be viewed
as ending a sentence. Car of each element is the character (a
string) and cdr a character that will be displayed when folded."
  :type '(alist :key-type string :value-type string)
  :set #'org-fold-linebreaks--set-re)

(defface org-fold-linebreaks-face
  '((nil (:inherit (bold font-lock-keyword-face))))
  "Face for displaying folded linebreaks.")

(define-minor-mode org-fold-linebreaks-mode
  "Fold linebreaks"
  :lighter ""
  (cond
   (org-fold-linebreaks-mode
    (unless (derived-mode-p 'org-mode)
      (user-error "Cannot activate linebreak-folding outside Org mode"))
    (org-fold-linebreaks--fold-region (point-min) (point-max))
    (add-hook 'change-major-mode-hook #'org-fold-linebreaks--clear nil t))
   (t
    (org-fold-linebreaks--clear)
    (remove-hook 'change-major-mode-hook #'org-fold-linebreaks--clear t))))

(defun org-fold-linebreaks ()
  "Re-fold new linebreaks."
  (interactive)
  (org-fold-linebreaks--fold-region (point-min) (point-max)))

(defun org-fold-linebreaks--clear ()
  "Remove all fold overlays in current buffer."
  (mapc #'delete-overlay org-fold-linebreaks--overlays)
  (setq org-fold-linebreaks--overlays nil))

(defun org-fold-linebreaks--fold-region (beg end)
  "Fold region from BEG to END."
  (interactive "r")
  (save-excursion
    (goto-char beg)
    (while (search-forward-regexp org-fold-linebreaks-re end t)
      (unless (save-excursion (beginning-of-line 0)
                              (looking-at-p "[-*#]"))
        (org-fold-linebreaks--overlay-put
         (match-beginning 1)
         (match-end 1)
         (concat (alist-get (match-string 2)
                            org-fold-linebreaks-sentence-end-punctuation
                            (match-string 2) nil #'equal)
                 (when (match-beginning 3)
                   (match-string 3))
                 " "))))))

(defun org-fold-linebreaks--overlay-put (from to rep)
  "Put overlay at FROM to TO, replace with REP."
  (let ((o (make-overlay from to nil 'front-advance)))
    (overlay-put o 'evaporate t)
    (overlay-put o 'display (propertize rep 'face 'org-fold-linebreaks-face))
    (overlay-put o
		         'isearch-open-invisible
		         (lambda (&rest _) (org-fold-show-context 'isearch)))
    (push o org-fold-linebreaks--overlays)))


(provide 'org-fold-linebreaks)
;;; org-fold-linebreaks.el ends here
